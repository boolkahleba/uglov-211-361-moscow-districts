<?php require('db_connection.php');
$dist_id = $_GET['selOption'];
$result = mysqli_query($connect, "SELECT * FROM `information` WHERE id = '" . $dist_id . "'");
$district = mysqli_fetch_array($result);
require("session.php");

//if (!empty($_POST)) {
//    $result1 = mysqli_query($connect, "UPDATE `information` SET
//    `count_gyms` = " . $_POST["cnt_g"] . ",
//    `count_playgrounds` = " . $_POST["cnt_pg"] . ",
//    `count_supermarkets` = " . $_POST["cnt_s"] . ",
//    `count_veterinary` = " . $_POST["cnt_v"] . ",
//    `fire_service_calls` = " . $_POST["fsc"] . ",
//    `police_calls` = " . $_POST["pc"] . "
//    WHERE `information`.`id` = '" . $dist_id . "';");
//
//    header("Location: edit.php");
//}

if(isset($_POST["cnt_g"])){
    $cnt_g = intval($_POST["cnt_g"]);
}
if(isset($_POST["cnt_pg"])){
    $cnt_pg = intval($_POST["cnt_pg"]);
}
if(isset($_POST["cnt_s"])){
    $cnt_s = intval($_POST["cnt_s"]);
}
if(isset($_POST["cnt_v"])){
    $cnt_v = intval($_POST["cnt_v"]);
}
if(isset($_POST["fsc"])){
    $fsc = $_POST["fsc"];
}
if (isset($_POST["pc"])) {
    $pc = $_POST["pc"];
    $result1 = mysqli_query($connect, 'UPDATE `information` SET
    `count_gyms` = ' . $cnt_g . ',
    `count_playgrounds` = ' . $cnt_pg . ',
    `count_supermarkets` = ' . $cnt_s . ',
    `count_veterinary` = ' . $cnt_v . ',
    `fire_safety` = "' . $fsc . '",
    `police_safety` = "' . $pc . '"
    WHERE information.id = ' . $dist_id . ';');

    header("Location: edit.php");
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Moscow Districts</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="./layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
    <link href="./layout/styles/preloader.css" rel="stylesheet" type="text/css" media="all">
    <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">

</head>

<body style="background-image: url("./images/backgrounds/moscow3.jpg"); background-size: 100%">
<?php include "./preloader.php" ?>
<header>
    <div class="navbar">
        <nav id="mainav" style="margin-left: 1em; color: white; background: none; border: none">
            <ul style="text-align: left">
                <li><a href="index.php">< На главную</a></li>
            </ul>
        </nav>
    </div>
</header>
<div style="display: flex; justify-content: center">
    <div class="edit">
        <form method="post">
            <p class="edit_header"><b>Изменить данные по району</b></p><br>

            <p class="edit_text1"><strong>Количество тренажерных залов: </strong>
                <input type="number" maxlength="15" size="25%" name="cnt_g"
                       value="<?php echo $district['count_gyms']; ?>"></p>

            <p class="edit_text1"><strong>Количество детских площадок в парках: </strong>
                <input type="number" maxlength="15" size="25%" name="cnt_pg"
                       value="<?php echo $district['count_playgrounds']; ?>"></p>

            <p class="edit_text1"><strong>Количество супермаркетов: </strong>
                <input type="number" maxlength="15" size="25%" name="cnt_s"
                       value="<?php echo $district['count_supermarkets']; ?>"></p>

            <p class="edit_text1"><strong>Количество ветеринарных клиник: </strong>
                <input type="number" maxlength="15" size="25%" name="cnt_v"
                       value="<?php echo $district['count_veterinary']; ?>"></p>

            <p class="edit_text1"><strong>Пожароопасность: </strong>
                <input type="text" maxlength="50" size="25%" name="fsc"
                       value="<?php echo $district['fire_safety']; ?>"></p>

            <p class="edit_text1"><strong>Криминальная опасность: </strong>
                <input type="text" maxlength="50" size="25%" name="pc"
                       value="<?php echo $district['police_safety']; ?>"></p>

            <div style="display: flex; justify-content: center">
                <button class="submitbutton" type="submit">Готово</button>
            </div>
        </form>
        <div class="reg">
            <p><a href="edit.php" class="auth_reg">Выбрать другой район</a></p>
        </div>
    </div>
</div>
<script src="./layout/scripts/preloader.js"></script>
</body>
</html>

<?php

?>