<?php
require("session.php");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Moscow Districts</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">

    <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
    <link href="layout/styles/preloader.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<?php include "./preloader.php" ?>
<header>
    <div style="color: white">
        <nav id="mainav">

            <?php
            if ($session_user != false) {
                $content = '<ul style="display: inline; padding-left: 30%">
                <li class="active"><a href="index.php">Главная</a></li>
                <li><a href="map.php">Карта</a></li>
                <li><a href="feedback.php">Обратная связь</a></li>';
                $user_role = $session_user['role'];
                if ($user_role == 'admin') {
                    $content = $content . '<li><a href="edit.php">Изменить данные</a></li></ul>';
                    $content = $content.'<ul style="display: inline; padding-left: 30%">
                <li><a href="session_end.php">Выйти</a></li></ul>';
                }else{
                    $content = $content.'</ul><ul style="display: inline; padding-left: 30%">
                <li><a href="session_end.php">Выйти</a></li></ul>';
                }

            } else {
                $content = '<ul style="display: inline; padding-left: 30%">
                <li class="active"><a href="index.php">Главная</a></li>
                <li><a href="map.php">Карта</a></li></ul>
            <ul style="display: inline; padding-left: 30%">
            <li><a href="authorisation.php">Войти</a></li></ul>';
            }
            echo $content;

            ?>
        </nav>
    </div>
</header>
<div class="bgded" style="background-image:url('images/backgrounds/moscow2.jpg');">
    <div class="wrapper overlay">
        <article id="pageintro" class="hoc clear">

            <h3 class="heading">Лучший район Москвы для вашей жизни</h3>
            <p>Найдите самое подходящее место для приобретения дома</p>
            <div><a class="btn" href="map.php">Найти</a></div>

        </article>
    </div>

</div>

<div class="wrapper row3">
    <section class="hoc container clear">

        <div class="sectiontitle">
            <h6 class="heading">Наши преимущества</h6>
            <br>
            <p>Мы предлагаем шесть критериев выбора района для жизни</p>
        </div>
        <ul class="nospace group elements">
            <li class="one_third first">
                <article><i class="fa-solid fa-children"></i>
                    <h6 class="heading">Наличие детских площадок</h6>
                    <p>Найдите район, в котором ваш ребёнок сможет гулять на игровых площадках</p>

                </article>
            </li>
            <li class="one_third">
                <article><i class="fa-solid fa-fire"></i>
                    <h6 class="heading">Частота пожаров</h6>
                    <p>Узнайте, является ли пожароопасным район вашего будущего жилища</p>
                </article>
            </li>
            <li class="one_third">
                <article><i class="fa fa-paw"></i>
                    <h6 class="heading">Наличие ветклиник</h6>
                    <p>Важный критерий, если у Вас есть домашнее животное. Чем ближе ветклиника, тем быстрее питомец
                        получит помощь</p>
                </article>
            </li>
            <li class="one_third first">
                <article><i class="fa-sharp fa-solid fa-basket-shopping"></i>
                    <h6 class="heading">Количество супермаркетов</h6>
                    <p>Удобно, когда всё необходимое можно купить рядом с домом</p>
                </article>
            </li>
            <li class="one_third">
                <article><i class="fa-sharp fa-solid fa-building-shield"></i>
                    <h6 class="heading">Частота обращений в полицию</h6>
                    <p>Узнайте, входит ли выбранный вами район в число криминально опасных</p>
                </article>
            </li>
            <li class="one_third">
                <article><i class="fa-solid fa-dumbbell"></i>
                    <h6 class="heading">Наличие тренажерных залов</h6>
                    <p>Найдите район, в котором сможете заниматься спортом в специальном зале</p>
                </article>
            </li>
        </ul>

    </section>
</div>

<div class="wrapper row4">
    <footer id="footer" class="hoc clear">
        <div style="display: flex; ">
            <p class="footer_text">Приложение составленно на основе данных за 2022 год из открытых источников:</p>
            <ul class="footer_links">
                <li>
                    <a href="https://data.mos.ru/opendata/7710881420-statsionarnye-torgovye-obekty/passport?versionNumber=1&releaseNumber=159">Стационарные
                        торговые объекты Москвы</a></li>
                <li>
                    <a href="https://data.mos.ru/opendata/7725570674-veterinarnye-uchrejdeniya/passport?versionNumber=4&releaseNumber=202">Ветеринарные
                        учреждения Москвы</a></li>
                <li>
                    <a href="https://data.mos.ru/opendata/7708308010-zaly-trenajernye/passport?versionNumber=4&releaseNumber=24">Места
                        для занятий спортом и отдыха Москвы</a></li>
                <li>
                    <a href="https://data.mos.ru/opendata/7708308010-detskie-igrovye-ploshchadki-v-parkah/passport?versionNumber=9&releaseNumber=2">Детские
                        игровые площадки в парках Москвы</a></li>
                <li>
                    <a href="https://data.mos.ru/opendata/7704776297-dannye-po-kolichestvu-obrashcheniy-grajdan-i-kollektivnyh-jalob-postupivshih-v-obshchestvennye-punkty-ohrany-poryadka-v-gorode-moskve-po-administrativnym-okrugam/passport?versionNumber=1&releaseNumber=65">Данные
                        по количеству обращений граждан Москвы в пункты охраны порядка</a></li>
                <li>
                    <a href="https://data.mos.ru/opendata/7710474791-dannye-vyzovov-pojarnoy-slujby-po-ao-goroda-moskvy/passport?versionNumber=1&releaseNumber=103">Данные
                        вызовов подразделений пожарно-спасательного гарнизона города Москвы по административным
                        округам</a></li>
            </ul>
        </div>
    </footer>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>

<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<script src="layout/scripts/preloader.js"></script>
<script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
</body>
</html>