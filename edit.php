<?php require('db_connection.php');
$result = mysqli_query($connect, 'SELECT * FROM `information`');
require("session.php");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Moscow Districts</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="./layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
    <link href="./layout/styles/preloader.css" rel="stylesheet" type="text/css" media="all">
    <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">

</head>

<body style="background-image: url('./images/backgrounds/moscow3.jpg'); background-size: 100%"
">
<?php include "./preloader.php" ?>
<header>
    <div class="navbar">
        <nav id="mainav" style="margin-left: 1em; color: white; background: none; border: none">
            <ul style="text-align: left">
                <li><a href="index.php">< На главную</a></li>
            </ul>
        </nav>
    </div>
</header>
<div style="display: flex; justify-content: center">
    <div class="auth">
        <?php
        $sel = $_POST['selOption'];
        ?>
        <form action="edit2.php?res=<?php echo $sel;?>" method="get">
            <p class="edit_header"><b>Изменить данные по району</b></p><br>
            <p class="auth_text"><strong>Выберите район: </strong>
            <div class="dropdown1">
                <select id="select" name="selOption">
<?php
$text = '';
while ($district = mysqli_fetch_array($result)) {
    $text = $text.'<option  name= "name" value="'.$district['id'].'">'.$district['name'].'</option>';
}
echo $text?>
                </select>
            </div>
            <br>
            <div style="display: flex; justify-content: center">
                <button class="submitbutton" type="submit">Далее</button>
            </div>
        </form>
    </div>
</div>
<script src="./layout/scripts/preloader.js"></script>
</body>
</html>
